import matplotlib.lines as mlines
import matplotlib.pyplot as plt
import pandas as pd


def plot_roc_hasSSM(em_property="HasSSM"):
    filename_ssm = "ssm_conf_matrix.hdf5"

    conf_matrix_gstlal = pd.read_hdf(filename_ssm, "gstlal_ssm")
    conf_matrix_mbta = pd.read_hdf(filename_ssm, "MBTA_ssm")

    fig, ax = plt.subplots(1, 1, figsize=(7, 5))
    cluster = ['o', '^', 'x']
    values = [0.10, 0.50, 0.90]
    labels = ['0.10', '0.50', '0.90']

    circle = mlines.Line2D([], [], color='blue', marker='o', linestyle='None',
                           markersize=5, label='0.1')
    triangle = mlines.Line2D([], [], color='blue', marker='^',
                             linestyle='None', markersize=5, label='0.5')
    cross = mlines.Line2D([], [], color='blue', marker='x', linestyle='None',
                          markersize=5, label='0.9')

    thresh_df = conf_matrix_gstlal[
        conf_matrix_gstlal['Threshold'].isin(values)]
    for xp, yp, m, v in zip(thresh_df['FPR'],
                            thresh_df['TPR'],
                            cluster, labels):
        ax.scatter([xp], [yp], marker=m, c='red', s=30)
    ax.plot(conf_matrix_gstlal['FPR'], conf_matrix_gstlal['TPR'],
            label="gstlal")
    thresh_df = conf_matrix_mbta[conf_matrix_mbta['Threshold'].isin(values)]
    for xp, yp, m, v in zip(thresh_df['FPR'], thresh_df['TPR'],
                            cluster, labels):
        ax.scatter([xp], [yp], marker=m, c='green', s=30)
    ax.plot(conf_matrix_mbta['FPR'], conf_matrix_mbta['TPR'], label="MBTA")
    custom1 = ax.legend(loc='lower right', prop={'size': 12})
    ax.legend(handles=[circle, triangle, cross], title='Threshold',
              loc='center right', prop={'size': 12})
    ax.add_artist(custom1)
    ax.set_xlim((0, 1))
    ax.set_ylim((0, 1))
    ax.set_title(em_property)
    ax.set_xlabel("FPR", fontsize=18)
    ax.set_ylabel("TPR", fontsize=18)
    ax.grid(linestyle='--')
    plt.show()


def plot_roc_hasNS(em_property="HasNS"):
    filename_ssm = "ssm_conf_matrix.hdf5"

    conf_matrix_gstlal = pd.read_hdf(filename_ssm, "gstlal_ns")
    conf_matrix_mbta = pd.read_hdf(filename_ssm, "MBTA_ns")

    fig, ax = plt.subplots(1, 1, figsize=(7, 5))
    cluster = ['o', '^', 'x']
    values = [0.10, 0.50, 0.90]
    labels = ['0.10', '0.50', '0.90']

    circle = mlines.Line2D([], [], color='blue', marker='o', linestyle='None',
                           markersize=5, label='0.1')
    triangle = mlines.Line2D([], [], color='blue', marker='^',
                             linestyle='None', markersize=5, label='0.5')
    cross = mlines.Line2D([], [], color='blue', marker='x', linestyle='None',
                          markersize=5, label='0.9')

    thresh_df = conf_matrix_gstlal[
        conf_matrix_gstlal['Threshold'].isin(values)]
    for xp, yp, m, v in zip(thresh_df['FPR'],
                            thresh_df['TPR'],
                            cluster, labels):
        ax.scatter([xp], [yp], marker=m, c='red', s=30)
    ax.plot(conf_matrix_gstlal['FPR'], conf_matrix_gstlal['TPR'],
            label="gstlal")
    thresh_df = conf_matrix_mbta[conf_matrix_mbta['Threshold'].isin(values)]
    for xp, yp, m, v in zip(thresh_df['FPR'], thresh_df['TPR'],
                            cluster, labels):
        ax.scatter([xp], [yp], marker=m, c='green', s=30)
    ax.plot(conf_matrix_mbta['FPR'], conf_matrix_mbta['TPR'], label="MBTA")
    custom1 = ax.legend(loc='lower right', prop={'size': 12})
    ax.legend(handles=[circle, triangle, cross], title='Threshold',
              loc='center right', prop={'size': 12})
    ax.add_artist(custom1)
    ax.set_xlim((0, 1))
    ax.set_ylim((0, 1))
    ax.set_title(em_property)
    ax.set_xlabel("FPR", fontsize=18)
    ax.set_ylabel("TPR", fontsize=18)
    ax.grid(linestyle='--')
    plt.show()


def plot_roc_hasMassGap(em_property="HasMassGap"):
    filename_ssm = "ssm_conf_matrix.hdf5"

    conf_matrix_gstlal = pd.read_hdf(filename_ssm, "gstlal_massgap")
    conf_matrix_mbta = pd.read_hdf(filename_ssm, "MBTA_massgap")

    fig, ax = plt.subplots(1, 1, figsize=(7, 5))
    cluster = ['o', '^', 'x']
    values = [0.10, 0.50, 0.90]
    labels = ['0.10', '0.50', '0.90']

    circle = mlines.Line2D([], [], color='blue', marker='o', linestyle='None',
                           markersize=5, label='0.1')
    triangle = mlines.Line2D([], [], color='blue', marker='^',
                             linestyle='None', markersize=5, label='0.5')
    cross = mlines.Line2D([], [], color='blue', marker='x', linestyle='None',
                          markersize=5, label='0.9')

    thresh_df = conf_matrix_gstlal[
        conf_matrix_gstlal['Threshold'].isin(values)]
    for xp, yp, m, v in zip(thresh_df['FPR'],
                            thresh_df['TPR'],
                            cluster, labels):
        ax.scatter([xp], [yp], marker=m, c='red', s=30)
    ax.plot(conf_matrix_gstlal['FPR'], conf_matrix_gstlal['TPR'],
            label="gstlal")
    thresh_df = conf_matrix_mbta[conf_matrix_mbta['Threshold'].isin(values)]
    for xp, yp, m, v in zip(thresh_df['FPR'], thresh_df['TPR'],
                            cluster, labels):
        ax.scatter([xp], [yp], marker=m, c='green', s=30)
    ax.plot(conf_matrix_mbta['FPR'], conf_matrix_mbta['TPR'], label="MBTA")
    custom1 = ax.legend(loc='lower right', prop={'size': 12})
    ax.legend(handles=[circle, triangle, cross], title='Threshold',
              loc='center right', prop={'size': 12})
    ax.add_artist(custom1)
    ax.set_xlim((0, 1))
    ax.set_ylim((0, 1))
    ax.set_title(em_property)
    ax.set_xlabel("FPR", fontsize=18)
    ax.set_ylabel("TPR", fontsize=18)
    ax.grid(linestyle='--')
    plt.show()

