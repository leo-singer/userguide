Data Analysis
=============

In this section we describe the different online searches looking for GW
signals, the selection and vetting of candidates, and parameter estimation
analysis.

When multiple candidates from different pipelines are close enough together in
time, they will be considered as originating from the same physical event and
will be grouped into a single :doc:`superevent <superevents>`. See the
following pages for technical details.

.. toctree::
   :maxdepth: 1

   searches
   superevents
   vetting
   parameter_estimation
   inference

The timeline for distribution of alerts is described below.

Alert Timeline
--------------

Here, we describe the sequence of LIGO/Virgo/KAGRA alerts for a single event
that will be distributed through the Gamma-ray Coordinates
Network (:term:`GCN`) via notices and circulars (see the :doc:`/content` and
:doc:`/tutorial/index` sections for details).

.. plot::
    :alt: Timeline for sending gravitational-wave alerts

    from astropy.visualization import quantity_support
    from astropy import units as u
    from matplotlib import pyplot as plt
    from matplotlib import rcParams
    from matplotlib.transforms import blended_transform_factory
    import numpy as np

    quantity_support()

    gradient = np.linspace(0, 1, 256)[np.newaxis, :]**0.5

    plot_data = [[['Sky Localization', -20 * u.second, 5 * u.second],
                  ['Classification', -20 * u.second, 5 * u.second],
                  ['Detection', -50  * u.second, 30 * u.second]],
                 [['Sky Localization', 30 * u.second, 5 * u.second],
                  ['Classification', 30 * u.second, 5 * u.second],
                  ['Automated Vetting', 30 * u.second, 5 * u.second],
                  ['Detection', 16 * u.second, 14 * u.second]],
                 [['Re-annotate', 3.6 * u.min, 1 * u.min],
                  ['Cluster additional events', 0.8 * u.min, 2.8 * u.min]],
                 [['Classification', 4 * u.hour, 10 * u.minute],
                  ['Human Vetting', 5 * u.minute, 4 * u.hour],
                  ['Parameter Estimation', 75 * u.second, 4 * u.hour]],
                 [['Classification', 6 * u.day, 6 * u.hour],
                  ['Parameter Estimation', 4 * u.hour, 6 * u.day]]]

    alert_labels = ['Early Warning\nAlert Sent',
                    '1st Preliminary\nAlert Sent',
                    '2nd Preliminary\nAlert Sent',
                    'Initial Alert or\nRetraction Sent',
                    'Update\nAlert Sent']
    bar_height = 0.8

    xlim = [-10 * u.minute, 100 * u.day]

    fig, axs = plt.subplots(
        len(plot_data),
        sharex=True,
        figsize=(8, 4),
        gridspec_kw=dict(
            height_ratios=[len(_) + 1 - bar_height for _ in plot_data],
            top=0.9, left=0, right=1, hspace=0.05, bottom=0.1
        ))

    for ax, data, alert_label, props in zip(axs, plot_data, alert_labels, rcParams['axes.prop_cycle']):
        ax.spines['left'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.set_yticks([])
        ax.xaxis.label.set_visible(False)
        ax.set_facecolor('0.95')

        labels, starts, durations = zip(*data)
        starts = u.Quantity(starts).to(u.second)
        durations = u.Quantity(durations).to(u.second)

        t = max(starts + durations)
        if t.value > 0:
            tp = t * 1.1
            tp2 = t * 1.25
            tp10 = 10*t
        else:
            tp = t + 3 * u.second
            tp2 = t + 8 * u.second
            tp10 = t + 40 * u.second
        ax.axvline(tp, color='black')
        ax.text(tp2, 0.5, alert_label,
                transform=blended_transform_factory(ax.transData, ax.transAxes),
                fontweight='bold', va='center', ha='left')

        ax.barh(np.arange(len(labels)), width=durations,
                left=starts, height=bar_height,
                facecolor=props['color'], edgecolor='black')
        for i, (start, duration, label) in enumerate(zip(starts, durations, labels)):
            ax.text(start, i,
                    ' ' + label + ' ', ha='right', va='center')
        ax.set_ylim(0.5 * bar_height - 1, len(labels) - 0.5 * bar_height)
        ax.imshow(gradient, extent=[tp.value, tp10.value, -10, 10], cmap='Greys_r', vmin=-1, vmax=1, aspect='auto')
        ax.axvspan(tp10, xlim[1], color='white')

    fig.suptitle('Time relative to gravitational-wave merger')
    ax.set_xscale('symlog', linthresh=40)
    ax.set_xlim(-10 * u.minute, 100 * u.day)
    ticks = [-30 * u.second, 0 * u.second, 30 * u.second, 3 * u.minute, 1 * u.hour, 1 * u.day, 1 * u.week]
    ax.set_xticks(ticks)
    ax.set_xticklabels([
        '{0.value:g} {0.unit.short_names[0]}'.format(_) if abs(_) < 1 * u.minute else
        '{0.value:g} {0.unit.long_names[0]}'.format(_)
        for _ in ticks
    ])
    ax.minorticks_off()
    ax.set_xlabel('Time since GW signal')
    axs[-1].arrow(0, 0, 1, 0,
                  transform=ax.transAxes, clip_on=False,
                  head_width=0.1, head_length=0.01,
                  linewidth=axs[-1].spines['bottom'].get_linewidth(),
                  edgecolor=axs[-1].spines['bottom'].get_edgecolor(),
                  facecolor=axs[-1].spines['bottom'].get_edgecolor(),
                  length_includes_head=True)
    for ax in axs[:-1]:
        plt.setp(ax.xaxis.get_major_ticks(), visible=False)


**Beginning from 1 minute before the GW merger time**, an
:doc:`early warning search </early_warning>` may find a pre-merger
candidate. If it does, then an :doc:`Early Warning alert </content>` may be
sent before the GW merger time.

**Within 1–10 minutes after GW trigger time**, the first and second
:doc:`preliminary alerts</content>` will be sent fully autonomously. The
trigger will be immediately and publicly visible in the :term:`GraceDB`
database. Since the procedure is fully automatic, some preliminary alerts may
be retracted after human inspection for data quality, instrumental conditions,
and pipeline behavior.

**Within 24 hours after the trigger time** of any **significant**
gravitational-wave alerts (possibly within 4 hours for :term:`BNS`
or :term:`NSBH` sources), the :doc:`Initial or Retraction alert
and circular </content>` will be distributed. It will include an updated sky
localization and source classification. At this stage, the event will have been
vetted by human instrument scientists and data analysts. The candidate will
either be **confirmed** by an Initial alert and circular or withdrawn by a
Retraction alert and circular if the data quality is unsuitable.

:doc:`Update alerts and circulars </content>` are sent whenever the sky
localization area or significance accuracy improves (e.g. as a result of
improved calibration, glitch removal, or computationally deeper parameter
estimation). Updates will be sent up until the position is determined more
accurately by public announcement of an unambiguous counterpart. At that point,
there will be no further sky localization updates until the publication of the
event in a peer-reviewed journal.

**At any time**, we may promote a candidate to be a **significant**
gravitational-wave alert if it is compellingly associated with a
multimessenger signal (e.g. :term:`GRB`, core-collapse :term:`SN`). In this
case, :doc:`Initial alert and circulars </content>` will be distributed.

.. _alert-threshold:

Alert Threshold
---------------

Automated preliminary alerts correspond to :doc:`superevent <superevents>` with a
candidate trigger (from any pipeline) with a false alarm (:term:`FAR`)
of :math:`2.3 \times  10^{-5}` Hz (two per day) before considering the trials factor.
We divide gravitational-wave alerts into two categories:
(1) **low-significance** gravitational-wave alerts and
(2) **significant** gravitational-wave alerts.
The LIGO/Virgo/KAGRA collaboration performs human vetting and further analysis on
**significant** gravitational-wave alerts. These **significant** alerts indicate the
presence of a candidate with a false alarm rate (:term:`FAR`) of :math:`6.34
\times 10^{-8}` Hz (one per six months) for searches that target :term:`CBC`
sources and of :math:`1.1 \times 10^{-8}` Hz (one per three years) for burst
searches that target generic signals.

Early-Warning alerts correspond to :doc:`EarlyWarning </early_warning>` CBC
searches (**GstLAL EarlyWarning**, **MBTA EarlyWarning**, **PyCBC EarlyWarning**,
and **SPIIR EarlyWarning**) with a candidate false alarm (:term:`FAR`) of :math:`9.6 \times
10^{-8}` Hz (one per four months) and are marked as **significant**
gravitational-wave alerts.

We do not perform any further analysis following **low-significance**
gravitational-wave preliminary alerts or early warning alerts that
are not followed by a significant preliminary alert. We will perform
human vetting only for **significant** alerts. Initial, retraction, and
update alerts will be sent out only for :doc:`superevent <superevents>`
associated with **significant** alerts.

.. _alert-threshold-trial-factor:

False Alarm Rate for Alerts and Trials Factor
---------------------------------------------

The reported false alarm rates do not include the trials factor
from using multiple search pipelines. The trials factor is applied so
**significant** alerts are sent out at the expected false alarm rate, accounting
for the number of searches that are online at the time of the alert.
We are now operating five independent searches that target :term:`CBC` sources
(**GstLAL AllSky**, **MBTA AllSky**, **PyCBC AllSky**, **SPIIR AllSky**, and **cWB BBH**)
and two independent Burst searches that target generic signals
(**cWB AllSky** and **MLy**). There is also an external coincidence search that
targets **significant** candidates from all online pipelines (**RAVEN**).
We thus conservatively estimate a trial factor of **6** for :term:`stellar mass`
:term:`CBC` searches and **3** for Burst searches. Similarly we have two
independent :term:`SSM` :term:`CBC` searches (**GstLAL SSM** and **MBTA SSM**).
Hence we use a trials factor of 2 for :term:`SSM` search alerts.

The estimated rate of false alarms marked as **significant** is :math:`3.9
\times 10^{-7}` Hz (one per month) for :term:`CBC` searches
and :math:`3.2 \times 10^{-8}` (one per year) for Burst searches.

The estimated rate of false alarms marked as **low-significance** is up to
:math:`1.2\times 10^{-4}` Hz (ten per day) for :term:`CBC` searches
and up to :math:`4.6 \times 10^{-5}` (four per day) for Burst searches.

The number of searches may change during O4b, impacting the trials factor.
If this is the case, the threshold for alerts marked as **significant**
will be adjusted such that the estimated rate of false alarms stays one
per month. The threshold for alerts marked as **low-significance** will
remain two per day for each search.

