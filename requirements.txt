astropy-healpix
pygcn
fastavro
check-jsonschema
jsonschema[format]
gcn-kafka
healpy
hop-client
igwn-gwalert-schema
ligo.skymap
packaging
seaborn
setuptools-scm
sphinx >= 4.0.0,<8.2  # remove upper constraint when https://github.com/jbms/sphinx-immaterial/issues/409 is fixed
sphinx-tabs >= 1.1.11
sphinx-immaterial-igwn
tables
